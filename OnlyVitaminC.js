const items = require("./items");

const OnlyVitaminC = items.filter((element, index, array) => {
  return element.contains === "Vitamin C";
});

console.log(OnlyVitaminC);
