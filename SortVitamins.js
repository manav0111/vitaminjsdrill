const items = require("./items");

const SortVitamins = items.sort((a, b) => {
  let alength = a.contains.length;
  let blength = b.contains.length;

  return blength - alength;
});

console.log(SortVitamins);
