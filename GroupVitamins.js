const items = require("./items");

let GroupVitamins = {};

items.forEach((element, index, array) => {
  const vitaminsArray = element.contains.split(",");

  for (let index = 0; index < vitaminsArray.length; index++) {
    if (
      GroupVitamins.hasOwnProperty(vitaminsArray[index].trim().toLowerCase())
    ) {
      continue;
    } else {
      GroupVitamins[vitaminsArray[index].trim().toLowerCase()] = [];
    }
  }
});


items.forEach((element,index,array)=>{
    
    if(element.contains.includes("Vitamin C"))
    {
        GroupVitamins['vitamin c'].push(element.name);
    }
    if(element.contains.includes("Vitamin K"))
    {
        GroupVitamins['vitamin k'].push(element.name);
    }
    if(element.contains.includes("Vitamin A"))
    {
        GroupVitamins['vitamin a'].push(element.name);
    }
    if(element.contains.includes("Vitamin B"))
    {
        GroupVitamins['vitamin b'].push(element.name);
    }
    
    if(element.contains.includes("Vitamin D"))
    {
        GroupVitamins['vitamin d'].push(element.name);
    }
    
})

console.log(GroupVitamins);
